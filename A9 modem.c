#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_event.h>
#include <api_socket.h>
#include <api_network.h>
#include <api_debug.h>
#include "api_hal_gpio.h"
#include "api_inc_gpio.h"

static HANDLE socketTaskHandle = NULL;
static HANDLE semStart = NULL;
typedef struct
{
    char buff[1024];
    uint16_t buffLength;
    uint16_t resultLength;
}httpData_t;

void EventDispatch(API_Event_t* pEvent) {
    const Network_PDP_Context_t context = {
        //replace it with your SIM profile
        .apn        ="telkomsel",
        .userName   = "wap"    ,
        .userPasswd = "wap123"
    };
    switch(pEvent->id) {
        case API_EVENT_ID_NO_SIMCARD:
            Trace(1,"!!NO SIM CARD%d!!!!",pEvent->param1);
            break;

        case API_EVENT_ID_NETWORK_REGISTER_SEARCHING:
            Trace(1,"network register searching");
            break;
        
        case API_EVENT_ID_NETWORK_REGISTER_DENIED:
            Trace(1,"network register denied");
            break;
        
        case API_EVENT_ID_NETWORK_REGISTER_NO:
            Trace(1,"network register no");
            break;
        
        case API_EVENT_ID_NETWORK_REGISTERED_HOME:
        case API_EVENT_ID_NETWORK_REGISTERED_ROAMING: {
            uint8_t status;
            Trace(1,"network register success");
            bool ret = Network_GetAttachStatus(&status);
            if(!ret)
                Trace(1,"get attach staus fail");
            Trace(1,"attach status:%d",status);
            if(status == 0) {
                ret = Network_StartAttach();
                if(!ret) {
                    Trace(1,"network attach fail");
                }
            } else {
                
                Network_StartActive(context);
            }
            break;
        }
        case API_EVENT_ID_NETWORK_ATTACHED:
            Trace(1,"network attach success");
            Network_StartActive(context);
            break;

        case API_EVENT_ID_NETWORK_ACTIVATED:
            Trace(1,"network activate success");
            OS_ReleaseSemaphore(semStart);
            break;

        default:
            break;
    }
}

int16_t httpGet(char *ip, int port, const char *path, httpData_t *httpData){
    int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(fd < 0){
        Trace(1,"socket fail");
        return -1;
    }
    char* servInetAddr = ip;
    struct sockaddr_in sockaddr;
    memset(&sockaddr,0,sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    inet_pton(AF_INET,servInetAddr,&sockaddr.sin_addr);
    int ret = connect(fd, (struct sockaddr*)&sockaddr, sizeof(struct sockaddr_in));
    if(ret < 0){
        Trace(1,"socket connect fail");
        close(fd);
        return -1;
    }

    ret = send(fd, path, strlen(path), 0);
    if(ret < 0){
        Trace(1,"socket send fail");
        close(fd);
        return -1;
    }
    struct fd_set fds;
    struct timeval timeout={12,0};
    FD_ZERO(&fds);
    FD_SET(fd,&fds);
    uint16_t recvLen = 0;
    uint8_t flag = 0;
    while(!flag){
        ret = select(fd+1,&fds,NULL,NULL,&timeout);
        switch(ret) {
            case -1:
                Trace(1,"select error");
                flag = true;
                break;
            case 0:
                Trace(1,"select timeout");
                flag = true;
                break;
            default:
                if(FD_ISSET(fd,&fds)){
                    ret = recv( fd, httpData->buff+recvLen, httpData->buffLength, 0 );
                    recvLen += ret;
                    ret = recv( fd, httpData->buff+recvLen, httpData->buffLength, 0 );
                    recvLen += ret;
                    httpData->resultLength = recvLen;
                    Trace(1, "%d %s", recvLen, httpData->buff);
                    return 1;
                }
                break;
        }
    }
    close(fd);
    return -1;
}

void taskHttp(void *pData){
    API_Event_t* event=NULL;
    Trace(1,"====== waiting semaphore ======");
    semStart = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semStart,OS_TIME_OUT_WAIT_FOREVER);
    OS_DeleteSemaphore(semStart);

    GPIO_config_t button = {
        .mode         = GPIO_MODE_INPUT,
        .pin          = GPIO_PIN2,
        .defaultLevel = GPIO_LEVEL_HIGH
    };


    GPIO_EnablePower(GPIO_PIN2, true);
    GPIO_Init(button);

    Trace(1,"====== >>>>> Semaphore released <<<<< ======");
    httpData_t httpData;
    while(1) {
        GPIO_LEVEL status;
        if( GPIO_GetLevel(button, &status) ){
            if( !status ){

                // if using DNS
                // uint8_t ip[16];
                // memset(ip,0,sizeof(ip));
                // if(DNS_GetHostByName2(domain,ip) != 0) {
                //     Trace(1,"get ip error");
                //     return -1;
                // }
                Trace(1, "button pressed");
                memset( httpData.buff, 0, sizeof( httpData.buff ) );
                httpData.buffLength = sizeof( httpData.buff );
                httpGet("178.128.110.40", 8000, "GET /check HTTP/1.1\r\nHost: 178.128.110.40:8000\r\n\r\n", &httpData);
            }
        }
        Trace(1, "HTTP");
        OS_Sleep(1000);
    }
}

void mainTask(void *pData) {
    API_Event_t* event=NULL;
    OS_CreateTask( taskHttp, NULL, NULL, 1024, 0, 0, 0, "HTTP Handler");
    Trace(1,"====== main task ======");
    
    
    while(1){
        if(OS_WaitEvent(socketTaskHandle, (void**)&event, OS_TIME_OUT_WAIT_FOREVER)) {
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}

void app_Main(void){
    socketTaskHandle = OS_CreateTask(mainTask, NULL, NULL, 4096, 0, 0, 0,"socket Test");
    OS_SetUserMainHandle(&socketTaskHandle);
}


